
/* This is a test file to run various scenarios. It is not important.
Use as an example.  It is run via index.js usin esm module.
Run from command line `npm run index.js`
 */

console.log("TEST LOADER");
import ActionWarden from './src/lib/action-warden';
const micros = require('microseconds');

const arbac = new ActionWarden();


const rule = {
	key: 'post',
	role: 'tester',
	action: ['read','write'],
	state: ['new','normal']
};

// arbac.addLogic('L1',{ //JSONLogic rule.
// 	"if" : [{"var": ["data.a",1] },true,false]
// });
arbac.addRule(rule);

let start = micros.now();





const user = {login:"a.tester.robot",roles:['tester','admin']};
const testdata = {"a":1, "b":"foo","x":"y", "sky":"blue"};
const smState = 'normal';


let allowed = arbac.isAllowed('post',user.roles,'read',testdata,smState);


let diff = micros.now() - start;
console.log('allowed = ', allowed);

console.log('Benchmark took ', micros.parse(diff), 'microseconds');


console.log('-=----=---- Time for 1000 runs:');

start = micros.now() - start;
for(let i= 0; i< 1000; i++){
	let x = arbac.isAllowed('post',user.roles,'read',testdata,smState);
}
diff = micros.now() - start;
console.log('Benchmark for 1000 runs took ', micros.parse(diff), 'microseconds');

console.log('-=----=---- Time for 1,000,000 runs:');
start = micros.now();
for(var i= 0; i< 1000000; i++){
	let x = arbac.isAllowed('post',user.roles,'read',testdata,smState);
}
diff = micros.now() - start;
console.log('Benchmark for 1,000,000 runs took ', micros.parse(diff), 'microseconds');

console.log("END TEST");
