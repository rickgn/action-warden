# Action Warden
_Attribute and Rules Based Access Controller_

* Roles based access control 
* Atribute based access control, using logic expressions on data
* State machine based contextual access control
* Rules based logic expressions that can be saved and reused
* Multiple expression syntax support

##### Roadmap

*TODO*: as of version 0.1.0 only one expression engine is supported at a time. The project
plan is to allow for simultaneous use, as well as a way to plug in any custom 
expression engine. 

*TODO*: Support for fetching data async as part of the isAllowed() method. 
This would include an async isAllowed method which can read remote data and
make a decision without necessarily storing results.

*TODO*: Caching async isAllowed results for a TTL period


### Abstracted rules expression engine 
Action Warden allows for using your own logic expression syntax. Examples in this document
are using [JSONLogic](http://jsonlogic.com) expression engine.
Example using JSONLogic:

`{"==" : [{"var": "data.a"},1] }`

Other expression engines could include [Mingo](https://github.com/kofrasa/mingo) 
Example using Mingo:

`{ "item.name": { $eq: "ab" }`

Both of the above expression syntax are meant to be serializable in JSON, allowing
for code portability and for storing logic expressions in a database. 

## Example Usage
There are several ways to use Action Warden. The simpliest way is to use
key, role and action only. 



### Non expression example
```
import ActionWarden from 'action-warden';
const actionWarden = new ActionWarden();

let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
};

actionWarden.addRule(rule);
let allowed = actionWarden.isAllowed('todos',user.roles,'read');
console.log('allowed=',allowed);
```

### Wildcard matches
So that you don't have to repeatedly write the same rule for multiple objects or roles, the wildcard '*'
is available. This is only available on key and role. Example:

```
let rule = {
	key: '*',
	role: '*',
	action: 'read',
	logicRef: 'myStoredLogic',
	invert: true
};
```

### Load multiple rules

In the following example, a ruleset is defined to:
 * allow read access to every role on posts
 * allow write access to admin role on all objects
 
Loading rules as an array is simple.

```
let rules = [
    {key: 'todos', role: '*',action: 'read'},
    {key: '*', role: 'admin',action: 'write'}
]
actionWarden.addRules(rules);
```

### Data expression example

If you need something more complex and conditional, the logic attribute in the rule
can be used. Logic is an Object normally, but is polymorphic and can also be
boolean or a custom function. 

This example is using JSONLogic expression engine. 
```
import ActionWarden from 'action-warden';
const actionWarden = new actionWarden();

let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
	// logic using JSONLogic syntax
	logic: {"==" : [{"var": "data.a"},1] }
};

actionWarden.addRule(rule);
let allowed = actionWarden.isAllowed('todos',user.roles,'read',{a:1});
console.log('allowed=',allowed);
```

You must take care to ensure that the expression doesn't return string, number or object values.
If it does, the function will actually be returning 'true' because all values are
converted to 'truthy' using standard JS boolean logic.  

### function expression example

In some cases you might want to do a one-off quick expression and use a function.
The plus-side is that this is fast. The negative side is that functions are not
serializable or portable between systems and other programming languages.
However, functions in logic are available if you need it.

Note that the context object contains `data` and `state`
Anything retruned from function will be 'truthy' meaning it will be evaluated
via standard JS boolean logic. Strings, objects, etc. will be converted to true/false

```
import ActionWarden from 'action-warden';
const actionWarden = new ActionWarden();

let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
	
};

actionWarden.addRule(rule);
let allowed = actionWarden.isAllowed('todos',user.roles,'read',{a:1},'new');
console.log('allowed=',allowed);
```
 
#### Example using data and state
```
let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
	logic: {"and": [
	  {"==" : [{"var": "data.a"},1] },
	  {"in" : [{"var": "state"},['filled','new', 'staged']] }
	]}
};
actionWarden.addRule(rule);
let allowed = actionWarden.isAllowed('todos',user.roles,'read',{a:1});
``` 
In this example, both expressions will need to be true because of the 'and' operator: 
`a=1` and state 'in' one of ('filled', 'new' or 'staged'). 

As you can see the context object includes both 'data' and 'state'. This design is specifically 
to allow for additional context varables to be introduced in the roadmap.

#### But that's too hard! 
For a shortcut to access to data state without using an expression, there is a shortcut.
```
let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
	state: ['filled','new']
};
```
OR:
`state: 'new'`

OR: 
`state: [5, 20, 30]`

The state attribute can be either String, Integer or Array. So for example if your state machine uses numbers
instead of state names, you can use the numbers the same way as the String names.

*NOTE*: You can only use 'state' exclusive to logic and logicRef. This is because using both
could cause an ambiguous logic statement which could conflict. Both 'state' attribute and 'context.state' have access
to the same state variable. This type of condition is notoriously difficult to debug, so it is not allowed.
See logic evaluation order   
 
### Stored logic expression example

In some caes it makes sense to store the logic once and refer to it later.
Instead of building a custom rule builder that injects the same logic repeatedly,
this function does exactly that. For each rule, logicRef inserts a wrapped function
that will call a predefined logic, using the syntax and stored logic of your choice.

NOTE: `logicRef` is always a String and will take priority over `logic`. They cannot both exist
in the same rule.

```
actionWarden.addLogic('myStoredLogic',{"==" : [{"var": "data.a"},1] });

let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
	logicRef: 'myStoredLogic'
};

actionWarden.addRule(rule);
let allowed = actionWarden.isAllowed('todos',user.roles,'read',{a:1});
console.log('allowed=',allowed);
```

### Invert rule
In some cases it makes sense to create the inverse rule of an existing one.

```
actionWarden.addLogic('myStoredLogic',{"==" : [{"var": "data.a"},1] });
let rule = {
	key: 'todos',
	role: 'admin',
	action: ['read','write'],
	logicRef: 'myStoredLogic',
	invert: true
};
``` 
The `invert` flag will cause the rule to flip the result from true to false.
This is used for situations where 'not allowed' is used instead of 'allowed'. 
It also makes sense for when you want to use the same stored logic for opposite things.
For example, if user has admin role, and if user does not have admin role.
Or if `a < 1` and if `a >= 1`.   


## Logic evaluation order
Due to the huge amount of flexibility of this rules engine, a hierarchy of 
evaluation was created to help avoid doing things that are difficult to diagnose.
In other words, it is designed to help you not do stupid stuff. ;-)

1. Logic ref takes priority. If it exists, try this first.
2. If not logic ref, look for 'logic' attribute to be true or false boolean.
3. If logic attribute is a function, use it.
4. If logic is an inline object, wrap it with an executor function using the 
preconfigured logic expression engine (such as JSONLogic or Mingo)
5. If none of the above then...

    A. If 'state is an array, evaluate
    
    B. If 'state' is a string or a number, evaluate with '==='. Note that this means "2" is not equal to 2.
    Evaluation here is not picky if it is an integer or a decimal.  3.4 will work but 3.4.5 will not,
    unless it is a string '3.4.5'. 
        
6. Finally, if none of the above (no logic, logicRef, or state) then assume is a simple
rule that is roles based only. Evaluate user in role only.

It is important to note that `logic: null` is not the same as logic attribute not present.    
If any other case than outlined above, there will be a console.error message and the 
rule will not be loaded, effectively not allowing the use case in isAllowed()
