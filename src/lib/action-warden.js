/**
 * Vue-js Attribute and Rules Based Access Control
 * Internally rules are stored in a hierarchy of
 * KEY -> ROLE -> ACTION
 * If rules include multiple actions, the rule is automatically split into multiple rules
 * Performance is optimized during the load process by creating functions for each unique rule
 * regardless of the logic type. For example, simple 'true' or a full JSONLogic expression
 * are evaluated in exactly the same way, as a function.
 *
 * Logic runner for expressions is abstracted per instance. By default JSONLogic is used.
 * However, you could use other expression engines such as mango, etc.
 *
 */
import JsonLogic from '@jeremywall/json-logic-js';
import {isString,isObject,isFunction} from 'lodash'
const isArray = Array.isArray;

const POLICY_FAIL = 'fail';
const POLICY_PASS = 'pass';

const LOGICRUNNER = JsonLogic.apply;


class ActionWarden{

	/**
	 *
	 * @param config {Array | Object} if array then calls addRules([])
	 */
	constructor(config){

		this._defaultPolicy = POLICY_FAIL;
		this._useDefaultPath = true;
		this._storedLogic = {};

		this._logicRunner = LOGICRUNNER;  // direct access to runner function

		this._rulesets = {
			'*': { // named resource
				'*': { // named role
					'*': { // named action
						logic: false, // one or more action functions.
						// all functions are signature fn(context)
						// these are build with load and loadAll
						// uses function for fastest execution time
					}
				}
			}
		};

		if(isArray(config)){
			this.addRules(config);
		}
		else if(isObject(config)){

			let {defaultPolicy, implyDefault, rules} = config;

			if(isArray(rules))
				this.addRules(rules);
			if(implyDefault === false){
				this._useDefaultPath = false;
			}
		}
	}

	_getLogicByPath(key = '*',role = '*',action = '*'){
		let keyRef, roleRef, kra;
		keyRef = this._rulesets[key];
		if(!keyRef && this._useDefaultPath && keyRef!=='*')
			keyRef =  this._rulesets['*'];
		if(keyRef){
			roleRef = keyRef[role];
			if(!roleRef && this._useDefaultPath && roleRef !== '*')
				roleRef = keyRef['*'];
			if(roleRef){
				kra = roleRef[action] || role['*'];
				return kra || false; // if found returns [...]
			}
		}
		return false;
	}

	_addLogicByPath(rule){
		let {key = '*',role = '*',action = '*'
			,logicRef,logic,invert,because,i18nBecause,state} = rule;
		let k, kr, kra;
		k = this._rulesets[key];
		if(!k){
			k = this._rulesets[key] = {};
		}
		kr = k[role];
		if(!kr){
			kr = k[role] = {};
		}
		kra = kr[action];
		if(!kra){
			kra = kr[action] = {logicRef,logic,invert,state,because,i18nBecause};
			this._buildLogicForRule(kra);
		}
	}

	// TODO: needs to throw error or something that can be caught that signfies 'do not allow'
	// simply returning false does not satisfy needs. This needs to be three-state logic
	// possibly return 1, 0, -1?  (design under consideration)

	_buildLogicForRule(rule){
		let {logic,logicRef,invert,state} = rule;
		if(isString(logicRef)){
			let storedLogic = this._storedLogic[logicRef];

			// TODO: Not sure which method is faster - the fn or the obj eval.
			if(isFunction(storedLogic)){
				rule.$fn = (context)=>{
					let result = storedLogic(context);
					if(invert) return !result;
					return result;
				}
			}

			else if(isObject(storedLogic)){
				//TODO: this is in evaluation. Might delete later. @deprecated
				console.warn("Deprecated rule method in use!");
				rule.$fn = (context)=>{
					let result = this._logicRunner(storedLogic,context);
					if(invert) return !result;
					return result
				};
			}
			else {
				console.warn("key: ", rule.key, "reference to stored logic",logicRef,"; is not stored in this instance");
				rule.$fn = function(){return false;}
			}
		}
		else if(logic === true){
			rule.$fn= function(){return true;}
		}
		else if(logic === false){
			rule.$fn= function(){return false;}
		}
		else if(isFunction(logic)){
			rule.$fn = (context)=>{
				let result = logic(context);
				if(invert) return !result;
				return result
			}
		}
		else if(isObject(logic)){
			rule.$fn = (context)=>{return this._logicRunner(logic,context) && !invert};
		}

		// by default, no logic and no logicRef assumes 'true' outcome
		// whenever there is a match to key, role and action.
		else if(logic === undefined && logicRef === undefined){
			if(state !== undefined){
				if(isArray(state)){
					rule.$fn= function(context){
						let result =  (state.indexOf(context.state) > -1);
						if(invert) return !result;
						return result;
					}
				}
				else if(isString (state) || ! isNaN(state)){
					rule.$fn= function(context){
						let result =  context.state === state;
						if(invert) return !result;
						return result;
					}
				}
			}
			else {
				rule.$fn= function(){return true;}
			}
		}
		else{
			console.error("invalid rule configuration on 'logic' attribute. See documentation. Rule not added",rule)
		}
	}

	/**
	 *
	 * @param rule
	 */
	addRule(rule){
		if(isArray(rule)) return this.addRules(rule);

		let multiRule = this._splitMultiAction(rule);
		if(multiRule) multiRule.forEach((rule)=>{
			this._addLogicByPath(rule);
		});

		else return this._addLogicByPath(rule);
	}

	// used for cases where action is array ['read','write','delete'] etc.
	// splits each action into separate, independent rule
	_splitMultiAction(rule){
		if(isArray(rule.action)){
			let ruleset = [];
			rule.action.forEach((act)=>{
				let newrule = {...rule};
				newrule.action = act;
				ruleset.push(newrule);
			});
			return ruleset;
		}
		else return false;
	}

	/**
	 * Add an array of rules
	 * @param rules {Array<Object>}
	 */
	addRules(rules){
		rules.forEach((rule)=>{
			let multiRule = this._splitMultiAction(rule);
			if(multiRule) multiRule.forEach((rule)=>{
				this._addLogicByPath(rule);
			});
			else this._addLogicByPath(rule);
		});
	}

	/**
	 * Used mostly for testing
	 * @param key {String}
	 * @returns {boolean}
	 */
	hasKey(key){
		return !! this._rulesets[key];
	}

	/**
	 * Used mostly for testing
	 * @param key {String}
	 * @param role {String}
	 * @returns {boolean}
	 */
	hasKeyRole(key,role){
		let keyref = this._rulesets[key];
		if(keyref) return !! keyref[role];
		return false;
	}

	/**
	 * Used mostly for testing
	 * @param key {String}
	 * @param role {String}
	 * @param action {String}
	 * @returns {boolean}
	 */
	hasKeyRoleAction(key,role,action){
		let keyref = this._rulesets[key];
		if(keyref){
			let roleref = keyref[role];
			if(roleref){
				return !!roleref[action]
			}
		}
		return false;
	}

	/**
	 *
	 * @param key {Object} the object key
	 * @param action {String} name of the action. Can include a dot ('.') path if using fields.
	 * For example 'user.profile.email', or whatever you want. Delimiters in path don't do anything but make the action unique.
	 * There is no runtime signifigance.
	 * @param roles {Array|String} array of roles that the user has, or just a single String role name
	 * @param {Object | *} [data]  Optional if using data context for expressions. Only used with logic rules.
	 * @param {String} [state] Optional. State machine state value. Only used with logic rules
	 * @returns {boolean}
	 */
	isAllowed(key,roles,action,data,state){
		const context = {action,data,state};
		if(isArray(roles)){

			let len = roles.length, role, logix;
			for(let i = 0; i< len; i++){
				role = roles[i];
				logix = this._getLogicByPath(key,role,action);
				if(logix){
					let result = logix.$fn(context);
					if(!!result) return true;
				}
			}
		}
		else {
			let logix = this._getLogicByPath(key,roles,action);
			return logix.$fn(context);
		}

		// if rule doesn't exist, fail by default.
		return  (this._defaultPolicy !== POLICY_FAIL);
	}

	/**
	 * Converts the logic to an executable function in isAllowed().
	 * Designed so that the actual logic runner is abstract.
	 * By default uses JSONLogic format, but this is controlled in constructor or in constants.
	 * @param name {String} name to store the logic as
	 * @param logic
	 */
	addLogic(name,logic){

		this._storedLogic[name] = (context)=>{
			return this._logicRunner(logic,context)
		};
		//this._storedLogic[name] = logic;
	}

	/**
	 * Tests to see if stored logic name key exists. True if exists
	 * @param name {String} name of the stored logic
	 * @returns {boolean}
	 */
	hasStoredLogic(name){
		return !! this._storedLogic[name];
	}



}

export default ActionWarden;
