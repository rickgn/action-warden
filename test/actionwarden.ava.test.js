/*
* TEST CASES FOR warden ( Attribute and Roles Based Access Controller )
*/
import ActionWarden from '../src/lib/action-warden';
import test from 'ava';
import {isObject, isString, isArray, isFunction} from 'lodash';


test.beforeEach(t => {
	let context = {
		user: {login:"a.tester.robot",roles:['tester','admin']},
		testdata: {a:1, b:'foo',x:'y', sky:'blue'},
		testlogicSimple: { //JSONLogic rule.
			"==": [  {"var": "data.a"}, 1]
		},
		testlogicMultiData: { //JSONLogic rule.
			"and": [
				{"==": [  {"var": "data.a"}, 1]},
				{"==": [  {"var": "data.sky"}, 'blue']},
			]
		},
		testlogicWithState: { //JSONLogic rule.
			"and": [
				{"==": [  {"var": "data.a"}, 1]},
				{"in":[ {"var": "state"}, ["normal", "ready", "approved"] ]}
			]
		}
	};
	context.warden = new ActionWarden();
	context.warden.addLogic('testlogicSimple',context.testlogicSimple);
	context.warden.addLogic('testlogicMultiData',context.testlogicMultiData);
	context.warden.addLogic('testlogicWithState',context.testlogicWithState);
	t.context = context;
});



test('Basic test to see if warden is an object', t => {
	t.true(t.context.warden instanceof ActionWarden);
});

test("should have saved logic defined in beforeEach", t => {
	t.true(t.context.warden.hasStoredLogic('testlogicSimple'));
});


test('has key', t => {
	const rule = {key: 'post', role: 'tester', action: 'read'};
	const warden = t.context.warden;
	warden.addRule(rule);
	t.true(warden.hasKey(rule.key));
});

test('has key role', t => {
	const rule = {key: 'post', role: 'tester', action: 'read'};
	const warden = t.context.warden;
	warden.addRule(rule);
	t.true(warden.hasKeyRole(rule.key, rule.role));
});

test('has key role action', t => {
	const rule = {key: 'post', role: 'tester', action: 'read'};
	const warden = t.context.warden;
	warden.addRule(rule);
	t.true(warden.hasKeyRoleAction(rule.key, rule.role, rule.action));
});

test('has key role action with multi-action input', t => {
	const rule = {key: 'post', role: 'tester', action: ['read','write']};
	const warden = t.context.warden;
	warden.addRule(rule);
	t.true(warden.hasKeyRoleAction(rule.key, rule.role, rule.action[1]));
});

test("Basic isAllowed test", t => {
	const rule = {key: 'post', role: 'tester', action: ['read','write']};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
	t.true(warden.isAllowed('post',user.roles,'write',testdata,'normal'));

});
test("Negative test case isAllowed", t => {
	const rule = {key: 'post', role: 'tester', action: ['read','write']};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'XXX',testdata,'normal'));
	t.false(warden.isAllowed('post',user.roles,'YYY',testdata,'normal'));

});

test("isAllowed with single condition logic", t => {
	const logicRefName = 'testlogicSimple';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logic: t.context.testlogicSimple
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));

});
test("NEGATIVE isAllowed with single condition logic", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logic: t.context.testlogicSimple
	};
	const warden = t.context.warden;
	const user = t.context.user;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',{valid:false},'normal'));

});

test("isAllowed with single condition logic REF", t => {
	const logicRefName = 'testlogicSimple';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logicRef:logicRefName
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));

});
test("NEGATIVE isAllowed with single condition logic REF", t => {
	const logicRefName = 'testlogicSimple';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logicRef:logicRefName
	};
	const warden = t.context.warden;
	const user = t.context.user;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',{valid:false},'normal'));

});


test("multi test condition with testlogicMultiData stored logic", t => {
	const logicRefName = 'testlogicMultiData';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logicRef:logicRefName
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
});

test("isAllowed testing with state in context using testlogicWithState rule", t => {
	const logicRefName = 'testlogicWithState';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logicRef:logicRefName
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));

});

test("NEGATIVE is allowed against state", t => {
	const logicRefName = 'testlogicWithState';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logicRef:logicRefName
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',testdata,'NOTNORMAL'));

});

test("Function as logic", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logic: (context)=>{
			return (context.data.a === 1);
		}
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'))
});
test("NEGATIVE Function as logic", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logic: (context)=>{
			return (context.data.a !== 1);
		}
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',testdata,'normal'))
});

test("inverted rule on direct logic", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logic: t.context.testlogicSimple,
		invert: true
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
});

test("inverted rule on stored logic", t => {
	const logicRefName = 'testlogicSimple';
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logicRef:logicRefName,
		invert: true
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',{valid:false},'normal'));
});

test("inverted rule on function", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		logic: (context)=>{
			return (context.data.a === 1);
		},
		invert:true
	};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',testdata,'normal'))
});


test("basic rule with state", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		state: 'normal'
	};
	const warden = t.context.warden;
	const user = t.context.user;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',{},'normal'));
});
test("NEGATIVE basic rule with state", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		state: 'normal'
	};
	const warden = t.context.warden;
	const user = t.context.user;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',{},'abnormal'));
});
test("basic rule with state ARRAY", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		state: ['new','normal']
	};
	const warden = t.context.warden;
	const user = t.context.user;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',{},'normal'));
});
test("NEGATIVE basic rule with state ARRAY", t => {
	const rule = {
		key: 'post',
		role: 'tester',
		action: ['read','write'],
		state: ['new','normal']
	};
	const warden = t.context.warden;
	const user = t.context.user;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',{},'abnormal'));
});

test("wildcard object with fixed role using simple rule", t => {
	const rule = {key: '*', role: 'tester', action: ['read','write']};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
	t.true(warden.isAllowed('post',user.roles,'write',testdata,'normal'));
});
test("NEGATIVE wildcard object with fixed role using simple rule", t => {
	const rule = {key: '*', role: 'nothing', action: ['read','write']};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.false(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
});

test("wildcard role with fixed key using simple rule", t => {
	const rule = {key: '*', role: 'tester', action: ['read','write']};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
	t.true(warden.isAllowed('other_object',user.roles,'write',testdata,'normal'));
});

test("wildcard role with wildcard key using single action string rule", t => {
	const rule = {key: '*', role: '*', action: 'read'};
	const warden = t.context.warden;
	const user = t.context.user;
	const testdata = t.context.testdata;
	warden.addRule(rule);
	t.true(warden.isAllowed('post',user.roles,'read',testdata,'normal'));
	t.false(warden.isAllowed('post',user.roles,'write',testdata,'normal'));
});
